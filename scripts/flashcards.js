window.onload = function() {
	document.getElementById( "header-title" ).addEventListener( "click", function() {
		window.location.href = ".";
	}, false);

	loadJSON( "configs/header-menu.json", loadMainMenu );

	checkIsPortrait();
	window.onorientationchange = checkIsPortrait;
};

/*
 Similar to jQuery's $.getJSON()
 path     = Absolute path to JSON file
 callback = Function that accepts JSON object as parameter, do not include "()"
 data     = Additional data to send to callback function

 Sample:
 loadJSON( "configs/words.json", buildWords, "português" );
 */
function loadJSON( path, callback, data ) {
	var xhttp = new XMLHttpRequest();
	xhttp.overrideMimeType( "application/json" );
	xhttp.open( "GET", path, true );
	xhttp.onreadystatechange = function () {
		if ( xhttp.readyState == 4 ) {
			switch (xhttp.status) {
				case 200:
					callback( JSON.parse( xhttp.responseText ), data );
					break;
				case 404:
					alert( "Sorry. Something went wrong." );
					// Track HTTP 404 for Google Analytics (if GA tag is used on index.html)
					ga( 'send', 'event', 'Errors', 'HTTP 404', path );
					break;
			}

			var dt = new Date();
			console.log( "Fetch " + path + " (" + dt.toUTCString() + "): " + xhttp.status + " " + xhttp.statusText );
		}
		// Do not place any code here. It will execute multiple times, as readState goes from 2, 3, 4.
	};
	xhttp.send(null);
}

// response from header-menu.json config file
function loadMainMenu( response ) {
	var headerUl = document.createElement( "ul" );
	headerUl.id = "header-menu";

	response.headerMenu.forEach( function( entry ) {
		var headerLi = document.createElement( "li" );
		var img = document.createElement( "img" );
		img.src = entry.img;
		img.alt = entry.title;
		img.className = "header-menu-img";
		img.id = entry.htmlId;

		headerLi.appendChild( img );
		headerUl.appendChild( headerLi );
	});

	headerUl.addEventListener( "click", loadActivity, false );

	var header = document.getElementById( "header" );
	header.appendChild( headerUl );
}

function loadActivity( e ) {
	if ( e.target !== e.currentTarget ) {
		switch( e.target.id ) {
			case "words-english":
				loadJSON( "configs/words.json", buildWords, "english" );
				break;
			case "words-français":
				loadJSON( "configs/words.json", buildWords, "français" );
				break;
			case "words-português":
				loadJSON( "configs/words.json", buildWords, "português" );
				break;
			case "fullscreen":
				toggleFullScreen();
				break;
		}
	}
	e.stopPropagation();
}

// response from words.json config file
function buildWords( response, data ) {
	var menuUl = document.createElement( "ul" );
	menuUl.id = "leftMenu";

	response.words[data].forEach( function( entry ) {
		var menuLi = document.createElement( "li" );
		var text = document.createTextNode( entry );
		menuLi.className = "leftMenuItem";
		menuLi.id = data + "-" + entry;
		menuLi.appendChild( text );
		menuUl.appendChild( menuLi );
	});

	menuUl.addEventListener( "click", loadContent, false );

	var leftColumn = document.getElementById( "leftcolumn" );

	// Must test because first buildWords will not have children to remove
	while ( leftColumn.hasChildNodes() )
		leftColumn.removeChild( leftColumn.firstChild );

	leftColumn.appendChild( menuUl );

	var contentAudioSpan = document.createElement( "span" );
	contentAudioSpan.id = "contentaudio" ;
	document.getElementById( "content" ).appendChild( contentAudioSpan );
}

function loadContent( e ) {
	if ( e.target !== e.currentTarget ) {
		document.getElementById( "contenttext" ).innerHTML = e.target.textContent;
		document.getElementById( "contentaudio" ).innerHTML = "<img src=\"media/speakword.png\" alt=\"Speak Word\"  id=\"audiobutton\">";

		var pos = e.target.id.indexOf("-");
		var language = e.target.id.substring( 0, pos );
		var word = e.target.id.substring( pos + 1 );
		var playAudio = function() {
			var audio = new Audio( "media/audio/" + language + "/" + word + ".mp3");
			audio.play();
		};

		// I had to re-add the <button> to the page for each word to reset the event listener
		// removeEventListener() wasn't working for new calls to the loadContent() function
		document.getElementById( "audiobutton" ).addEventListener( "click", playAudio, false );

		// Track menu item clicks for Google Analytics (if GA tag is used on index.html)
		ga( 'send', 'event', 'Click Interactions', 'Flash Card Word', e.target.id );
	}
	e.stopPropagation();
}

// Does not work on iOS :|
function toggleFullScreen() {
	if (!document.fullscreenElement &&    // alternative standard method
		!document.mozFullScreenElement && !document.webkitFullscreenElement && !document.msFullscreenElement ) {  // current working methods
		if (document.documentElement.requestFullscreen) {
			document.documentElement.requestFullscreen();
		} else if (document.documentElement.msRequestFullscreen) {
			document.documentElement.msRequestFullscreen();
		} else if (document.documentElement.mozRequestFullScreen) {
			document.documentElement.mozRequestFullScreen();
		} else if (document.documentElement.webkitRequestFullscreen) {
			document.documentElement.webkitRequestFullscreen();
		}
	} else {
		if (document.exitFullscreen) {
			document.exitFullscreen();
		} else if (document.msExitFullscreen) {
			document.msExitFullscreen();
		} else if (document.mozCancelFullScreen) {
			document.mozCancelFullScreen();
		} else if (document.webkitExitFullscreen) {
			document.webkitExitFullscreen();
		}
	}
}

function checkIsPortrait() {
	// Need to detect device because rotation detection varies
	// Could have declared this as global variable but was messy
	var os = "";
	if( navigator.userAgent.match(/(iPad|iPhone|iPod)/i) ) {
		os = "iOS";
	} else if( navigator.userAgent.match(/Android/i) ) {
		os = "android";
	}

	if ( os == "iOS" && Math.abs( window.orientation ) !== 90 ||
		os == "android" && Math.abs( window.orientation ) === 90 ) {
		alert( "Rotate device to landscape for best results" );
	}
}
