# Flash Cards

Digital flash cards for my son to practice reading and math. Standard, paper flash cards are fine. But this allows me to access these flash cards on my tablet wherever we are, regardless if I remembered to bring paper flash cards.

_Note: Should use in landscape mode and better fit for tablets than phones._

# Demo Link

http://iangetz.com/flashcards/